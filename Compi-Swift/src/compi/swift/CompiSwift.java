/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compi.swift;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 *
 * @author Jostin Marín
 * @author Juan José Guerrero
 */
public class CompiSwift {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, Exception {
        
        System.out.println("------------------");
        new Parser(new Scanner(new FileInputStream("prueba.txt"))).parse();
      
     
    }
    
}

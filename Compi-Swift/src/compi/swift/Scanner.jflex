
package compi.swift;
import java_cup.runtime.Symbol;

%%
%cupsym Sym
%class Scanner
%cup
%public
%unicode
%line
%char
%ignorecase

%{
    StringBuffer string = new StringBuffer();

    private Symbol symbol(int type) {

        return new Symbol(type, yyline, yycolumn);

    }

    private Symbol symbol(int type, Object value) {

        return new Symbol(type, yyline, yycolumn, value);

    }
%}

numeroEntero 	=      0 | [1-9][0-9]*
identificador =     [A-Za-z_][A-Za-z0-9]*
//operadores =          "^" | "?" | "~" | "&" | "%" | "|"
%%

/* key words */

"para"              { return symbol(Sym.para); }
"donde"              { return symbol(Sym.donde); }
"atraves"              { return symbol(Sym.atraves); }
"guardia"              { return symbol(Sym.guardia); }
"cambiar"              { return symbol(Sym.cambiar); }
"como"              { return symbol(Sym.como); }
"cualquier"              { return symbol(Sym.cualquier); }
"atrapar"              { return symbol(Sym.atrapar); }
"relanzar"              { return symbol(Sym.relanzar); }
"superior"              { return symbol(Sym.superior); }
"asimismo"              { return symbol(Sym.asimismo); }
"lanzar"              { return symbol(Sym.lanzar); }
"lanza"              { return symbol(Sym.lanza); }
"tratar"              { return symbol(Sym.tratar); }
"tipoasociado"              { return symbol(Sym.tipoasociado); }
"clase"                     { return symbol(Sym.clase); }
"desinicializar"            { return symbol(Sym.desinicializar); }
"enumerable"                { return symbol(Sym.enumerable); }
"extencion"                 { return symbol(Sym.extencion); }
"archivoprivado"            { return symbol(Sym.archivoprivado); }
"funcion"                   { return symbol(Sym.funcion); }
"importar"                  { return symbol(Sym.importar); }
"iniciar"                   { return symbol(Sym.iniciar); }
"entsal"                    { return symbol(Sym.entsal); }
"interno"                   { return symbol(Sym.interno); }
"dejar"                     { return symbol(Sym.dejar); }
"abrir"                     { return symbol(Sym.abrir); }
"operador"                  { return symbol(Sym.operador); }
"privado"                   { return symbol(Sym.privado); }
"protocolo"                 { return symbol(Sym.protocolo); }
"publico"                   { return symbol(Sym.publico); }
"estatico"                  { return symbol(Sym.estatico); }
"estructura"                { return symbol(Sym.estructura); }
"suscrito"                  { return symbol(Sym.suscrito); }
"aliasdetipo"               { return symbol(Sym.aliasdetipo); }
"variable"                  { return symbol(Sym.variable); }
"romper"                    { return symbol(Sym.romper); }
"caso"                      { return symbol(Sym.caso); }
"continua"                  { return symbol(Sym.continua); }
"pordefecto"                { return symbol(Sym.pordefecto); }
"haga"                      { return symbol(Sym.haga); }
"hagadespues"               { return symbol(Sym.hacerdespues); }
"sino"                      { return symbol(Sym.sino); }
"si"                        { return symbol(Sym.si); }
"en"                        { return symbol(Sym.en); }
"repetir"                   { return symbol(Sym.repetir); }
"retornar"                  { return symbol(Sym.retornar); }
"mientras"                 { return symbol(Sym.mientras); }
"falso"                     { return symbol(Sym.falso); }
"verdadero"                 { return symbol(Sym.verdadero); }
"nulo"                      { return symbol(Sym.nulo); }
"es"                        { return symbol(Sym.es); }
"entero"                    { return symbol(Sym.entero); }
"hilera"                    { return symbol(Sym.hilera); }
"boleano"                   { return symbol(Sym.boleano); }
"arreglo"                   { return symbol(Sym.arreglo); }
"/"                         { return symbol(Sym.div); }
"+"                         { return symbol(Sym.suma); }
"-"                         { return symbol(Sym.resta); }
"*"                         { return symbol(Sym.mult); }
"!"                         { return symbol(Sym.neg); }
">"                         { return symbol(Sym.mayor); }
"<"                         { return symbol(Sym.menor); }
"="                         { return symbol(Sym.igual); }
"%"                         { return symbol(Sym.modulo); }
"("                         { return symbol(Sym.parCirIni); }
")"                         { return symbol(Sym.parCirFin); }
"["                         { return symbol(Sym.parCuaIni); }
"]"                         { return symbol(Sym.parCuaFin); }
"{"                         { return symbol(Sym.CorIni); }
"}"                         { return symbol(Sym.CorFin); }
";"                         { return symbol(Sym.puntoycoma); }
":"                         { return symbol(Sym.dospuntos); }
"=="                        { return symbol(Sym.comparar); }
">="                        { return symbol(Sym.mayorigual); }
"<="                        { return symbol(Sym.menorigual); }
"++"                        { return symbol(Sym.incremento); }
"--"                        { return symbol(Sym.decremento); }
","                         { return symbol(Sym.coma); }
"."                         { return symbol(Sym.punto); }


{numeroEntero}              { return symbol(Sym.numeroEntero, new String(yytext())); }
{identificador}             { return symbol(Sym.identificador, new String(yytext())); }

[ \t\r\f\n]+       { }

.   { throw new Error("Illegal character <" + yytext() + ">"); }
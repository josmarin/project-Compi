package compi; 

import arbol.TablaDeSimbolos;
import java.util.LinkedList;
import java_cup.runtime.*;
import arbol.Instruccion;
import arbol.Asignacion;
import arbol.Si;
import arbol.Declaracion;
import arbol.Simbolo.Tipo;
import arbol.Imprimir;
import arbol.Operacion;
import arbol.Simbolo;
import arbol.Mientras;

parser code 
{:
    
    public LinkedList<Instruccion> AST;
    
    public void syntax_error(Symbol s){ 
            System.err.println("Error Sintáctico en la Línea " + (s.left) +" Columna "+s.right+ ". No se esperaba este componente: " +s.value+"."); 
    } 
    
    public void unrecovered_syntax_error(Symbol s) throws java.lang.Exception{ 
            System.err.println("Error síntactico irrecuperable en la Línea " + (s.left)+ " Columna "+s.right+". Componente " + s.value + " no reconocido."); 
    }  
    
    public LinkedList<Instruccion> getAST() {
        return AST;
    }

:} 

terminal String PTCOMA,DOSPTS,PARIZQ,PARDER,LLAVIZQ,LLAVDER, COMA, NEG;
terminal String MAS,MENOS,POR,DIVIDIDO,CONCAT, MODULO;
terminal String MENQUE,MAYQUE;
terminal String ENTERO;
terminal String DECIMAL;
terminal String CADENA;
terminal String UMENOS;
terminal String RIMPRIMIR, RMIENTRAS,RNUMERO,RSI,RSINO;
terminal String IGUAL;
terminal String IDENTIFICADOR;
terminal String RINTEGER, RBOLEANO, RARREGLO, RHILERA, RFALSO, RVERDADERO, RDOBLE;

terminal String RPARA,RDONDE,RATRAVES,RGUARDIA;
terminal String RATRAPAR, RCAMBIAR,RCOMO,RCUALQUIER;
terminal String RRELANZAR, RSUPERIOR,RASIMISMO,RLANZAR;
terminal String RLANZA, RTRATAR,RTIPOASOCIADO,RCLASE,RDESINICIALIZAR;

terminal String RENUMERABLE, REXTENCION,RARCHIVOPRIVADO,RFUNCION,RIMPORTAR;
terminal String RINCIAR, RENTSAL,RINTERNO,RDEJAR,RABRIR;
terminal String ROPERADOR, RPRIVADO,RPROTOCOLO,RPUBLICO,RESTATICO;
terminal String RESTRUCTURA, RSUSCRITO,RALIASDETIEMPO,RVARIABLE,RROMPER;

terminal String RCASO, RCONTINUA,RPORDEFECTO,RHAGA,RHAGADESPUES;
terminal String REN, RREPETIR,RRETORNAR;


non terminal ini;
non terminal LinkedList<Instruccion> instrucciones;
non terminal Instruccion instruccion;
non terminal Operacion expresion_numerica;
non terminal Operacion expresion_cadena;
non terminal Operacion expresion_logica;
non terminal Simbolo.Tipo tipo;
non terminal String expresion_importar,tipo_importar, lista_cadena;
precedence left CONCAT;
precedence left MAS,MENOS;
precedence left POR,DIVIDIDO;
precedence right UMENOS; 

start with ini; 

ini::=instrucciones:a  {:parser.AST=a;:}
;

instrucciones ::= 
   instrucciones:ins instruccion:i{: RESULT=ins; RESULT.add(i); :}
 | instruccion:i{: RESULT= new LinkedList<>(); RESULT.add(i); :}
; 

instruccion ::= 
   RIMPRIMIR PARIZQ expresion_cadena:a PARDER                                   {:RESULT = new Imprimir(a);:}
 | RMIENTRAS PARIZQ expresion_logica:a PARDER LLAVIZQ instrucciones:b LLAVDER   {:RESULT = new Mientras(a,b); :}
 | RVARIABLE IDENTIFICADOR:a DOSPTS tipo:b                                      {:RESULT = new Declaracion(a,b); :}
 | RDEJAR IDENTIFICADOR:a DOSPTS tipo:b                                         {:RESULT = new Declaracion(a,b); :}
 | IDENTIFICADOR:a IGUAL expresion_cadena:b                                     {:RESULT = new Asignacion(a,b);:}
 | RSI PARIZQ expresion_logica:a PARDER LLAVIZQ instrucciones:b LLAVDER         {:RESULT = new Si(a,b); :}
 | RSI PARIZQ expresion_logica:a PARDER LLAVIZQ instrucciones:b LLAVDER  RSINO LLAVIZQ instrucciones:c LLAVDER {: RESULT = new Si(a,b,c); :}
 | expresion_importar                                                           
 | error
;

expresion_numerica  ::= 
    MENOS expresion_numerica:a                               {:RESULT = new Operacion(a,Operacion.TipoOperacion.NEGATIVO);:}%prec UMENOS     
 |  expresion_numerica:a MAS        expresion_numerica:b     {:RESULT = new Operacion(a,b,Operacion.TipoOperacion.SUMA);:}
 |  expresion_numerica:a MENOS      expresion_numerica:b     {:RESULT = new Operacion(a,b,Operacion.TipoOperacion.RESTA);:}
 |  expresion_numerica:a POR        expresion_numerica:b     {:RESULT = new Operacion(a,b,Operacion.TipoOperacion.MULTIPLICACION);:}
 |  expresion_numerica:a DIVIDIDO   expresion_numerica:b     {:RESULT = new Operacion(a,b,Operacion.TipoOperacion.DIVISION);:}
 |  PARIZQ expresion_numerica:a PARDER                       {:RESULT = a;:}
 |  ENTERO:a                                                 {:RESULT = new Operacion(new Double(a));:}
 |  DECIMAL:a                                                {:RESULT = new Operacion(new Double(a));:}
 |  IDENTIFICADOR: a                                         {:RESULT = new Operacion(a,Operacion.TipoOperacion.IDENTIFICADOR);:}
;

expresion_cadena::=
    expresion_cadena:a CONCAT expresion_cadena:b {:RESULT = new Operacion(a,b,Operacion.TipoOperacion.CONCATENACION);:}
 |  CADENA:a                                     {:RESULT = new Operacion(a,Operacion.TipoOperacion.CADENA);:}
 |  expresion_numerica:a                         {:RESULT=a;:}
 |  RVERDADERO:a                                 {:RESULT = new Operacion(a,Operacion.TipoOperacion.BOLEANO);:}
 |  RFALSO:a                                     {:RESULT = new Operacion(a,Operacion.TipoOperacion.BOLEANO);:}
 |  expresion_numerica:a MODULO   expresion_numerica:b        {:RESULT = new Operacion(a,b,Operacion.TipoOperacion.MODULACION);:}
;

expresion_logica  ::= 
   expresion_numerica:a  MAYQUE   expresion_numerica:b      {:RESULT = new Operacion(a,b,Operacion.TipoOperacion.MAYORQUE);:} 
 | expresion_numerica:a  MENQUE   expresion_numerica:b      {:RESULT = new Operacion(a,b,Operacion.TipoOperacion.MENORQUE);:}
 | expresion_numerica:a  IGUAL IGUAL   expresion_numerica:b {:RESULT = new Operacion(a,b,Operacion.TipoOperacion.IGUALES);:}
| expresion_numerica:a  NEG IGUAL   expresion_numerica:b {:RESULT = new Operacion(a,b,Operacion.TipoOperacion.DIFERENTE);:}  
 | RVERDADERO:a                                             {:RESULT = new Operacion(a,Operacion.TipoOperacion.VERDADERO);:}
 | RFALSO:a                                                 {:RESULT = new Operacion(a,Operacion.TipoOperacion.FALSO);:}
;

tipo ::= 
      RINTEGER:a    {: RESULT = Tipo.NUMERO; :}
    | RBOLEANO:a    {: RESULT = Tipo.BOLEANO; :}
    | RARREGLO:a    {: RESULT = Tipo.ARREGLO; :}
    | RHILERA:a     {: RESULT = Tipo.HILERA; :}
    | RDOBLE:a     {: RESULT = Tipo.DOBLE; :}
    ;

expresion_importar ::=
      RIMPORTAR tipo_importar:a CADENA:b         {:System.out.println("Se ha importado " + a + b);:}
    | RIMPORTAR CADENA:a                         {:System.out.println("Se ha importado " + a);:}
    | RIMPORTAR PARIZQ lista_cadena PARDER       {:System.out.println("Se ha importado varios elementos");:}
    ;

lista_cadena ::=
      CADENA  COMA lista_cadena 
    | CADENA
    ;

tipo_importar ::= 
      RALIASDETIEMPO 
    | RESTRUCTURA       {: RESULT = "la estructura "; :}
    | RCLASE:a          {: RESULT = "la clase "; :}
    | RENUMERABLE       {: RESULT = "el enumerable "; :}
    | RPROTOCOLO        {: RESULT = "el protocolo "; :}
    | RVARIABLE         {: RESULT = "la variable "; :}
    | RFUNCION          {: RESULT = "la funcion "; :}
    ;
package compi;
import java_cup.runtime.Symbol; 
import java.util.LinkedList;

%% 
%class Lexico
%public 
%line 
%char 
%cup 
%unicode
%ignorecase

%{
	public LinkedList<String> generatedTokens = new LinkedList<String>();
	public LinkedList<String> getTokens(){
		return generatedTokens;
	}

%}

%init{ 
    yyline = 1; 
    yychar = 1; 
%init} 
 
BLANCOS=[ \r\t]+
CADENACOMILLASDOBLES = [\"]([^\"\n]|(\\\"))*[\"]
ENTERO=[0-9]+
FLOTANTE=[0-9]+("."[  |0-9]+)?
ID=[A-Za-z]+["_"0-9A-Za-z]*
COMENTUNILINEA =("//".*\r\n)|("//".*\n)|("//".*\r)
COMENTMULTILINEA ="/*""/"*([^*/]|[^*]"/"|"*"[^/])*"*"*"*/"
%%

{COMENTUNILINEA} {} 
{COMENTMULTILINEA} {}  
{CADENACOMILLASDOBLES} {return new Symbol(sym.CADENA,yyline,yychar, (yytext()).substring(1,yytext().length()-1));} 

"imprimir" {generatedTokens.add(yytext()); return new Symbol(sym.RIMPRIMIR,yyline,yychar, yytext());} 
"numero" {generatedTokens.add(yytext()); return new Symbol(sym.RNUMERO,yyline,yychar, yytext());} 
"mientras" {generatedTokens.add(yytext()); return new Symbol(sym.RMIENTRAS,yyline,yychar, yytext());} 
//"if" {return new Symbol(sym.RIF,yyline,yychar, yytext());} 
//"else" {return new Symbol(sym.RELSE,yyline,yychar, yytext());}


"para"             {generatedTokens.add(yytext()); return new Symbol(sym.RPARA,yyline,yychar, yytext());}
"donde"              {generatedTokens.add(yytext());return new Symbol(sym.RDONDE,yyline,yychar, yytext());}
"atraves"              {generatedTokens.add(yytext());return new Symbol(sym.RATRAVES,yyline,yychar, yytext());}
"guardia"              {generatedTokens.add(yytext());return new Symbol(sym.RGUARDIA,yyline,yychar, yytext());}
"cambiar"              {generatedTokens.add(yytext());return new Symbol(sym.RCAMBIAR,yyline,yychar, yytext());}
"como"              {generatedTokens.add(yytext());return new Symbol(sym.RCOMO,yyline,yychar, yytext());}
"cualquier"              {generatedTokens.add(yytext());return new Symbol(sym.RCUALQUIER,yyline,yychar, yytext());}
"atrapar"              {generatedTokens.add(yytext());return new Symbol(sym.RATRAPAR,yyline,yychar, yytext());}
"relanzar"              {generatedTokens.add(yytext());return new Symbol(sym.RRELANZAR,yyline,yychar, yytext());}
"superior"              {generatedTokens.add(yytext());return new Symbol(sym.RSUPERIOR,yyline,yychar, yytext());}
"asimismo"              {generatedTokens.add(yytext());return new Symbol(sym.RASIMISMO,yyline,yychar, yytext());}
"lanzar"              {generatedTokens.add(yytext());return new Symbol(sym.RLANZAR,yyline,yychar, yytext());}
"lanza"              {generatedTokens.add(yytext());return new Symbol(sym.RLANZA,yyline,yychar, yytext());}
"tratar"              {generatedTokens.add(yytext());return new Symbol(sym.RTRATAR,yyline,yychar, yytext());}
"tipoasociado"              {generatedTokens.add(yytext());return new Symbol(sym.RTIPOASOCIADO,yyline,yychar, yytext());}
"clase"                     {generatedTokens.add(yytext());return new Symbol(sym.RCLASE,yyline,yychar, yytext());}
"desinicializar"            {generatedTokens.add(yytext());return new Symbol(sym.RDESINICIALIZAR,yyline,yychar, yytext());}
"enumerable"                {generatedTokens.add(yytext());return new Symbol(sym.RENUMERABLE,yyline,yychar, yytext());}
"extencion"                 {generatedTokens.add(yytext());return new Symbol(sym.REXTENCION,yyline,yychar, yytext());}
"archivoprivado"            {generatedTokens.add(yytext());return new Symbol(sym.RARCHIVOPRIVADO,yyline,yychar, yytext());}
"funcion"                   {generatedTokens.add(yytext());return new Symbol(sym.RFUNCION,yyline,yychar, yytext());}
"importar"                  {generatedTokens.add(yytext());return new Symbol(sym.RIMPORTAR,yyline,yychar, yytext());}
"iniciar"                   {generatedTokens.add(yytext());return new Symbol(sym.RINCIAR,yyline,yychar, yytext());}
"entsal"                    {generatedTokens.add(yytext());return new Symbol(sym.RENTSAL,yyline,yychar, yytext());}
"interno"                   {generatedTokens.add(yytext());return new Symbol(sym.RINTERNO,yyline,yychar, yytext());}
"dejar"                     {generatedTokens.add(yytext());return new Symbol(sym.RDEJAR,yyline,yychar, yytext());}
"abrir"                     {generatedTokens.add(yytext());return new Symbol(sym.RABRIR,yyline,yychar, yytext());}
"operador"                  {generatedTokens.add(yytext());return new Symbol(sym.ROPERADOR,yyline,yychar, yytext());}
"privado"                   {generatedTokens.add(yytext());return new Symbol(sym.RPRIVADO,yyline,yychar, yytext());}
"protocolo"                 {generatedTokens.add(yytext());return new Symbol(sym.RPROTOCOLO,yyline,yychar, yytext());}
"publico"                   {generatedTokens.add(yytext());return new Symbol(sym.RPUBLICO,yyline,yychar, yytext());}
"estatico"                  {generatedTokens.add(yytext());return new Symbol(sym.RESTATICO,yyline,yychar, yytext());}
"estructura"                {generatedTokens.add(yytext());return new Symbol(sym.RESTRUCTURA,yyline,yychar, yytext());}
"suscrito"                  {generatedTokens.add(yytext());return new Symbol(sym.RSUSCRITO,yyline,yychar, yytext());}
"aliasdetipo"               {generatedTokens.add(yytext());return new Symbol(sym.RALIASDETIEMPO,yyline,yychar, yytext());}
"variable"                  {generatedTokens.add(yytext());return new Symbol(sym.RVARIABLE,yyline,yychar, yytext());}
"romper"                    {generatedTokens.add(yytext());return new Symbol(sym.RROMPER,yyline,yychar, yytext());}
"caso"                      {generatedTokens.add(yytext());return new Symbol(sym.RCASO,yyline,yychar, yytext());}
"continua"                  {generatedTokens.add(yytext());return new Symbol(sym.RCONTINUA,yyline,yychar, yytext());}
"pordefecto"                {generatedTokens.add(yytext());return new Symbol(sym.RPORDEFECTO,yyline,yychar, yytext());}
"haga"                      {generatedTokens.add(yytext());return new Symbol(sym.RHAGA,yyline,yychar, yytext());}
"hagadespues"               {generatedTokens.add(yytext());return new Symbol(sym.RHAGADESPUES,yyline,yychar, yytext());}
"sino"                      {generatedTokens.add(yytext());return new Symbol(sym.RSINO,yyline,yychar, yytext());}
"si"                        {generatedTokens.add(yytext());return new Symbol(sym.RSI,yyline,yychar, yytext());}
"en"                        {generatedTokens.add(yytext());return new Symbol(sym.REN,yyline,yychar, yytext());}
"repetir"                   {generatedTokens.add(yytext());return new Symbol(sym.RREPETIR,yyline,yychar, yytext());}
"retornar"                  {generatedTokens.add(yytext());return new Symbol(sym.RRETORNAR,yyline,yychar, yytext());}
"entero"                    {generatedTokens.add(yytext());return new Symbol(sym.RINTEGER,yyline,yychar, yytext());}
"hilera"                    {generatedTokens.add(yytext());return new Symbol(sym.RHILERA,yyline,yychar, yytext());}
"boleano"                   {generatedTokens.add(yytext());return new Symbol(sym.RBOLEANO,yyline,yychar, yytext());}
"arreglo"                   {generatedTokens.add(yytext());return new Symbol(sym.RARREGLO,yyline,yychar, yytext());}
"falso"                     {generatedTokens.add(yytext());return new Symbol(sym.RFALSO,yyline,yychar, yytext());}
"verdadero"                 {generatedTokens.add(yytext());return new Symbol(sym.RVERDADERO,yyline,yychar, yytext());}
"doble"                     {generatedTokens.add(yytext());return new Symbol(sym.RDOBLE,yyline,yychar, yytext());}

/*
"es"                        { return symbol(Sym.es); }

*/


";" {generatedTokens.add(yytext()); return new Symbol(sym.PTCOMA,yyline,yychar, yytext());} 
":" {generatedTokens.add(yytext());return new Symbol(sym.DOSPTS,yyline,yychar, yytext());} 
"," {generatedTokens.add(yytext());return new Symbol(sym.COMA,yyline,yychar, yytext());} 
"{" {generatedTokens.add(yytext());return new Symbol(sym.LLAVIZQ,yyline,yychar, yytext());} 
"}" {generatedTokens.add(yytext());return new Symbol(sym.LLAVDER,yyline,yychar, yytext());} 
"(" {generatedTokens.add(yytext());return new Symbol(sym.PARIZQ,yyline,yychar, yytext());} 
")" {generatedTokens.add(yytext());return new Symbol(sym.PARDER,yyline,yychar, yytext());} 
"+" {generatedTokens.add(yytext());return new Symbol(sym.MAS,yyline,yychar, yytext());} 
"-" {generatedTokens.add(yytext());return new Symbol(sym.MENOS,yyline,yychar, yytext());} 
"*" {generatedTokens.add(yytext());return new Symbol(sym.POR,yyline,yychar, yytext());} 
"/" {generatedTokens.add(yytext());return new Symbol(sym.DIVIDIDO,yyline,yychar, yytext());} 
"&" {generatedTokens.add(yytext());return new Symbol(sym.CONCAT,yyline,yychar, yytext());} 
"<" {generatedTokens.add(yytext());return new Symbol(sym.MENQUE,yyline,yychar, yytext());} 
">" {generatedTokens.add(yytext());return new Symbol(sym.MAYQUE,yyline,yychar, yytext());} 
"=" {generatedTokens.add(yytext()); return new Symbol(sym.IGUAL,yyline,yychar, yytext());} 
"!" {generatedTokens.add(yytext());return new Symbol(sym.NEG,yyline,yychar, yytext());} 
"%" {generatedTokens.add(yytext());return new Symbol(sym.MODULO,yyline,yychar, yytext());} 

\n {yychar=1;}

{BLANCOS} {} 
{ID} {generatedTokens.add(yytext()); return new Symbol(sym.IDENTIFICADOR,yyline,yychar, yytext());} 
{ENTERO} {generatedTokens.add(yytext()); return new Symbol(sym.ENTERO,yyline,yychar, yytext());} 
{FLOTANTE} {generatedTokens.add(yytext()); return new Symbol(sym.DECIMAL,yyline,yychar, yytext());} 

. {

    System.err.println("Se presentó un error léxico, token no reconocido: "+yytext()+", en la linea: "+yyline+", en la columna: "+yychar);
}
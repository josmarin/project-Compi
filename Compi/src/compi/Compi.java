/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compi;

import java.io.FileInputStream;
import java.util.LinkedList;
import arbol.Instruccion;
import arbol.Simbolo;
import arbol.TablaDeSimbolos;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 *
 * @author juan
 */
public class Compi {

        private static TablaDeSimbolos currentTable;
        private static Lexico lexico;
        

	private static void interpretar(String path){
	Sintactico parser;
        LinkedList<Instruccion> ast;        //ARBOL SINTACTICO ABSTRACTO abstract sintax tree
        try {
            lexico = new Lexico(new FileInputStream(path));
            parser=new Sintactico(lexico);
            parser.parse();
            ast = parser.getAST();
            ejecutarAST(ast);

        } catch (Exception ex) {
            System.out.println("Error compilando el archivo");
        }
	}

    private static void ejecutarAST(LinkedList<Instruccion> pAst){
        if (pAst == null){
            System.out.println("Error durante la generacion del AST \n");
            return;
        }
        currentTable = new TablaDeSimbolos();
        for (Instruccion i : pAst){
            if (i != null){
                i.ejecutar(currentTable);
            }
        }

    }


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        String input;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (true){
            try{
                System.out.println("Digite la direccion del documento");
                System.out.print(">");
                input = br.readLine();
                if (input.equals("salir")){
                    break;
                }else if (input.equals("tabla")){
                    if (currentTable == null){
                        System.out.println("No existe documento cargado");
                    }else{
                        for (Simbolo sim : currentTable){
                            System.out.println("Simbolo : "+ sim.getId() + " = " + sim.getValor());
                        }
                        System.out.println("----------------------------------");
                    }
                }else if (input.equals("tokens")){
                    System.out.println("Simbolos generados : ");
                    for (String s : lexico.getTokens()){
                        System.out.println("Simbolo : "+ s);
                    }
                    System.out.println("Digite la palabra \"tabla\" para cargar la tabla de simbolos");
                    System.out.println("Digite la palabra \"tokens\" para cargar la tabla de simbolos");
                }else{
                    interpretar(input);
                    //"test/prueba9.txt"
                    
                    System.out.println("Digite la palabra \"tabla\" para cargar la tabla de simbolos");
                    System.out.println("Digite la palabra \"tokens\" para cargar la tabla de simbolos");
                }
            }catch (Exception ex) {
                System.out.println("Error leyendo la instruccion");
            }
        }
    }
    
    
    
    
}

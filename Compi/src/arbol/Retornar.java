package arbol;


public class Retornar implements Instruccion{

	private final Operacion valor;
    public Retornar(Operacion pValor){
        this.valor = pValor;
    }

    @Override
    public Object ejecutar(TablaDeSimbolos ts){
        return valor.ejecutar(ts);
    }


}
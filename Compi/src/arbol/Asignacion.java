package arbol;

public class Asignacion implements Instruccion{

	private final Operacion valor;
	private final String id;

	public Asignacion(String pId,Operacion pValor){
		this.id = pId;
		this.valor = pValor;

	}


	@Override
    public Object ejecutar(TablaDeSimbolos ts){
        ts.setValor(id,valor.ejecutar(ts));
        return null;
    }

}
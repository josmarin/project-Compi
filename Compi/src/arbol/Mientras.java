package arbol;

import java.util.LinkedList;


public class Mientras implements Instruccion{


    private final Operacion condicion;
    private final LinkedList<Instruccion> instrucciones;

    public Mientras(Operacion pCondicion,LinkedList<Instruccion> pInstrucciones){
        this.instrucciones = pInstrucciones;
        this.condicion = pCondicion;
    }

    @Override
    public Object ejecutar(TablaDeSimbolos ts){
        while ( (Boolean)condicion.ejecutar(ts) ){
            TablaDeSimbolos tablaLocal = new TablaDeSimbolos();
            tablaLocal.addAll(ts);
            for (Instruccion i : instrucciones){
                i.ejecutar(tablaLocal);
            }
        }
        return null;
    }


}
package arbol;
import java.util.LinkedList;
public class Funciones extends LinkedList<Funcion>{


    private static Funciones singleton = null;

    protected Funciones(){
        super();

    }

    public static Funciones getInstance() {
      if(singleton == null) {
         singleton = new Funciones();
      }
      return singleton;
   }

    public Funcion getFuncion(String id){
        for (Funcion f:this){
            if(f.getId().equals(id)){
                return f;
            }
        }
        System.out.println("La funcion " + id + " no existe");
        return null;
    }
    
	 
}
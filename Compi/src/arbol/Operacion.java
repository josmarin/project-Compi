package arbol;
public class Operacion implements Instruccion{
	
	 
    public static enum TipoOperacion{
        SUMA,
        RESTA,
        MULTIPLICACION,
        DIVISION,
        NEGATIVO,
        NUMERO,
        IDENTIFICADOR,
        CADENA,
        MAYORQUE,
        MENORQUE,
        CONCATENACION,
        BOLEANO,
        VERDADERO,
        FALSO,
        IGUALES,
        DIFERENTE,
        MODULACION
    }
    
    private final TipoOperacion tipo;
    
    private Operacion operadorIzq;
    
    private Operacion operadorDer;

    private Object valor;

    public Operacion(Operacion pOperadorIzq,Operacion pOperadorDer,TipoOperacion pTipoOPeracion){
    	this.tipo = pTipoOPeracion;
    	this.operadorDer = pOperadorDer;
    	this.operadorIzq = pOperadorIzq;

    }
    public Operacion(Operacion pOperadorIzq,TipoOperacion pTipoOPeracion){
    	this.tipo = pTipoOPeracion;
    	this.operadorIzq = pOperadorIzq;
    	
    }
    public Operacion(String pValor, TipoOperacion pTipoOPeracion){
    	this.tipo = pTipoOPeracion;
    	this.valor = pValor;

    }

    public Operacion(Double pValor){
    	this.valor = pValor;
    	this.tipo = TipoOperacion.NUMERO;
    }

    @Override
    public Object ejecutar(TablaDeSimbolos ts){
    	if (tipo == TipoOperacion.SUMA){
    		return (Double)operadorIzq.ejecutar(ts) + (Double)operadorDer.ejecutar(ts);
    	}else if (tipo == TipoOperacion.RESTA){
    		return (Double)operadorIzq.ejecutar(ts) - (Double)operadorDer.ejecutar(ts);
    	}else if (tipo == TipoOperacion.MULTIPLICACION){
    		return (Double)operadorIzq.ejecutar(ts) * (Double)operadorDer.ejecutar(ts);
    	}else if (tipo == TipoOperacion.DIVISION){
    		return (Double)operadorIzq.ejecutar(ts) / (Double)operadorDer.ejecutar(ts);
    	}else if (tipo == TipoOperacion.NEGATIVO){
    		return (Double)operadorIzq.ejecutar(ts) * -1;
    	}else if (tipo == TipoOperacion.NUMERO){
    		return new Double(valor.toString());
    	}else if (tipo == TipoOperacion.IDENTIFICADOR){
    		return ts.getValor(valor.toString());
    	}else if (tipo == TipoOperacion.CADENA){
    		return valor.toString();
    	}else if (tipo == TipoOperacion.MAYORQUE){
    		return ((Double)operadorIzq.ejecutar(ts)).doubleValue()>((Double)operadorDer.ejecutar(ts)).doubleValue();
    	}else if (tipo == TipoOperacion.MENORQUE){
    		return ((Double)operadorIzq.ejecutar(ts)).doubleValue()<((Double)operadorDer.ejecutar(ts)).doubleValue();
    	}else if (tipo == TipoOperacion.CONCATENACION){
    		return operadorIzq.ejecutar(ts).toString() + operadorDer.ejecutar(ts).toString();
    	}else if (tipo == TipoOperacion.BOLEANO){
    		return valor.toString();
    	}else if (tipo == TipoOperacion.VERDADERO){
    		return true;
    	}else if (tipo == TipoOperacion.FALSO){
    		return false;
    	}else if (tipo == TipoOperacion.IGUALES){
    		return ((Double)operadorIzq.ejecutar(ts)).doubleValue()==((Double)operadorDer.ejecutar(ts)).doubleValue();
    	}else if (tipo == TipoOperacion.DIFERENTE){
    		return ((Double)operadorIzq.ejecutar(ts)).doubleValue()!=((Double)operadorDer.ejecutar(ts)).doubleValue();
    	}else if (tipo == TipoOperacion.MODULACION){
    		return (Double)operadorIzq.ejecutar(ts) % (Double)operadorDer.ejecutar(ts);
    	}
        
        
        return null;
    }
	
}
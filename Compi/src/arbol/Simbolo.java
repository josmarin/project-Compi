package arbol;

public class Simbolo{


	private final Tipo tipo;
	private final String id;
	private Object valor;


	public static enum Tipo{
        NUMERO,
        HILERA,
        BOLEANO,
        ARREGLO,
        DOBLE
    }

	public Simbolo(String pId,Tipo pTipo){
		this.tipo = pTipo;
		this.id = pId;
	}


	public void setValor(Object pValor){
		this.valor = pValor;
	}

	public Object getValor(){
		return this.valor;
	}


	public String getId(){
		return this.id;
	}

	


}
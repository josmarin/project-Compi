package arbol;

public class Imprimir implements Instruccion{

	private final Instruccion valor;

	public Imprimir(Instruccion pValor){
		this.valor = pValor;
	}


	@Override
	public Object ejecutar(TablaDeSimbolos ts){
		System.out.println(valor.ejecutar(ts).toString());
        return null;
	}

}
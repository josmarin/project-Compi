package arbol;

import java.util.LinkedList;


public class Funcion{


    
    private final String id;
    private final LinkedList<Instruccion> instrucciones;
    private final LinkedList<Simbolo> parametrosRequeridos;
    private final Funcion.Tipo tipo;

    public static enum Tipo{
        VACIO,
        NUMERO,
        CADENA
    }


    public Funcion(String pId,LinkedList<Instruccion> pInstrucciones,LinkedList<Simbolo> pParametrosRequeridos,
        Tipo pTipo){
        this.instrucciones = pInstrucciones;
        this.parametrosRequeridos = pParametrosRequeridos;
        this.tipo = pTipo;
        this.id = pId;
    }

    public Funcion(String pId,LinkedList<Instruccion> pInstrucciones,LinkedList<Simbolo> pParametrosRequeridos){
        this.instrucciones = pInstrucciones;
        this.parametrosRequeridos = pParametrosRequeridos;
        this.tipo = Tipo.VACIO;
        this.id = pId;
    }

    public String getId(){
        return this.id;
    }

    public Object ejecutar(TablaDeSimbolos ts,LinkedList<Operacion> parametros){
        if (parametrosRequeridos.size() != parametros.size()){
            return null;
        }

        TablaDeSimbolos tablaLocal = new TablaDeSimbolos();
        for (int i = 0; i < parametrosRequeridos.size(); i ++){
            parametrosRequeridos.get(i).setValor(parametros.get(i).ejecutar(ts));
            tablaLocal.add(parametrosRequeridos.get(i));
        }

        for (Instruccion i: instrucciones){
            if (i instanceof Retornar){
                return i.ejecutar(tablaLocal);
            }
            i.ejecutar(tablaLocal);
        }

        return null;
    }


}
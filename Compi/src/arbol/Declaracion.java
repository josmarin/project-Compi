package arbol;


public class Declaracion implements Instruccion{


    private final String id;
    private Simbolo.Tipo tipo;
    

    public Declaracion(String pId,Simbolo.Tipo pTipo){
        this.id = pId;
        this.tipo = pTipo;
    }

    @Override
    public Object ejecutar(TablaDeSimbolos ts){
        ts.add(new Simbolo(id,tipo));
        return null;
    }


}
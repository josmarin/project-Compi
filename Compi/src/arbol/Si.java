package arbol;

import java.util.LinkedList;

public class Si implements Instruccion{


    private Operacion condicion;
    private LinkedList<Instruccion> instrucciones;
    private LinkedList<Instruccion> instruccionesSino;

    public Si(Operacion pCondicion,LinkedList<Instruccion> pInstrucciones){
        this.condicion = pCondicion;
        this.instrucciones = pInstrucciones;
    }

    public Si(Operacion pCondicion,LinkedList<Instruccion> pInstrucciones,LinkedList<Instruccion> pInstruccionesSino){
        this.condicion = pCondicion;
        this.instrucciones = pInstrucciones;
        this.instruccionesSino = pInstruccionesSino;
    }

    
    @Override
    public Object ejecutar(TablaDeSimbolos ts){
        if ((Boolean)condicion.ejecutar(ts)){
            TablaDeSimbolos tablaLocal = new TablaDeSimbolos();
            tablaLocal.addAll(ts);
            for (Instruccion i : instrucciones){
                i.ejecutar(tablaLocal);
            }
        }else{
            if (instruccionesSino != null){
                TablaDeSimbolos tablaLocal = new TablaDeSimbolos();
                tablaLocal.addAll(ts);
                for (Instruccion i : instruccionesSino){
                    i.ejecutar(ts);
                }
            }
            
        }

        return null;
    }


}